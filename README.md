# My take on Twitter

## Description
Twitter like app.
Some functionality: 
    - Renders tweet feed from js file.
    - User can like, retweet, reply to other's tweets.
    - User can create own tweets.
